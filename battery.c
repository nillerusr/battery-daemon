#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <syslog.h>
#include <string.h>

static void skeleton_daemon()
{
	pid_t pid;
	pid = fork();

	if (pid < 0)
		exit(EXIT_FAILURE);

	if (pid > 0)
		exit(EXIT_SUCCESS);

	if (setsid() < 0)
		exit(EXIT_FAILURE);

	signal(SIGCHLD, SIG_IGN);
	signal(SIGHUP, SIG_IGN);

	pid = fork();

	if (pid < 0)
		exit(EXIT_FAILURE);

	if (pid > 0)
		exit(EXIT_SUCCESS);

	umask(0);

	chdir("/");

	int x;
	for (x = sysconf(_SC_OPEN_MAX); x>=0; x--)
	{
		close (x);
	}
}
#define PATH_BATT_STATE "/sys/class/power_supply/battery/status"
#define PATH_BATT_CHARGE "/sys/class/power_supply/battery/capacity"
FILE *fstate,*fcharge;

struct led_t
{
	char name[10];
	int state;
};

struct led_t red={"red",1};
struct led_t green={"green",1};
struct led_t blue={"blue",1};

enum powerstate{
	POWER_FULL,
	POWER_CHARGING,
	POWER_DISCHARGING
};

void setLED(struct led_t *led, int state){
	if( led->state == state )
		return;

	FILE *fled;
	char ledq[100];
	snprintf(ledq,sizeof ledq,"/sys/class/leds/%s/brightness",led->name);
	fled=fopen(ledq,"w");
	fprintf(fled,"%d",state);
	led->state = state;
	fclose(fled);
}

int getBatteryState(){
	fstate=fopen(PATH_BATT_STATE,"r");
	char state[30];
	fscanf(fstate,"%s",state);
	fclose(fstate);
	if (strcmp(state,"Charging") == 0){
		return POWER_CHARGING;
	}
	else if (strcmp(state,"Discharging") == 0){
		return POWER_DISCHARGING;
	}
	else if (strcmp(state,"Full") == 0){
		return POWER_FULL;
	}
}

int getBatteryLevel(){
	int charge;

	fcharge=fopen(PATH_BATT_CHARGE,"r");
	fscanf(fcharge,"%d",&charge);
	fclose(fcharge);

	return charge;
}

int main(){
	int state,level;
	skeleton_daemon();

	setLED(&red,0);
	setLED(&green,0);
	setLED(&blue,0);

	while (1){
		state=getBatteryState();
		level=getBatteryLevel();

		if( state != POWER_CHARGING )
			setLED(&red,0);

		setLED(&green,0);
		setLED(&blue,0);

		sleep(2);

		if( level < 15 && state != POWER_CHARGING )
		{
			sleep(1);
			continue;
		}

		switch( state ){
			case POWER_CHARGING:
				setLED(&red,1);
				setLED(&green,0);
				setLED(&blue,0);
			break;
			case POWER_DISCHARGING:
				setLED(&green,0);
				setLED(&blue,1);
				setLED(&red,0);
			break;
			case POWER_FULL:
				setLED(&red,0);
				setLED(&blue,0);
				setLED(&green,1);
			break;
		}
		sleep(1);
	}
}
